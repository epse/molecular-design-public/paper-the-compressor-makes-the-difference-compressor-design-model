# Refrigerant Selection for Heat Pumps: the Compressor Makes the Difference
# Refrigerant-dependent compressor design model

In this repository, you can find the refrigerant-dependent compressor design model developed for the paper ["Refrigerant Selection for Heat Pumps: the Compressor Makes the Difference"](https://onlinelibrary.wiley.com/doi/10.1002/ente.202201403) by Lisa Neumaier, Dennis Roskosch, Johannes Schilling, Gernot Bauer, Joachim Gross, and André Bardow published in *Energy Technology*. 

The framework can be found [here](https://gitlab.ethz.ch/epse/molecular-design-public/paper-the-compressor-makes-the-difference).

The refrigerant-dependent compressor model implemented in this work is written in [Rust](https://www.rust-lang.org/) and has been compiled under Linux as a Python package. Tests on other platforms have not been performed.

## Installation

The installation of the compressor model requires the package [`maturin`](https://github.com/PyO3/maturin) which should be easily installable in the desired environment using

```
pip install maturin
```

Then, the Rust compressor code can be compiled as a Python package with

```
maturin develop --release
```
The desired enviroment must be activated during the compilation with `maturin`.

## Simulating the refrigerant-dependent compressor

In Python, the compressor model can be imported with

```
import detailed_compressor
```
The simulation is carried out using
```
simulation = detailed_compressor.Simulation(saft_param_values, joback_param_values, t_in, p_in, p_out, t_th_0, d,
                                            discretization_steps, max_error, max_iterations)
```

with the arguments:

- `saft_param_values`: Values of Saft parameters, list of length 7: `[m, sigma, epsilon_k, mu, kappa_ab, epsilon_k_ab, molarweight]`
- `joback_param_values`: Values of Joback parameters, list of length 5: `[a, b, c, d, e]` (*Optional*, default: QSPR method for ideal gas contribution)
- `t_in`: Temperature at compressor inlet, in Kelvin
- `p_in`: Pressure at compressor inlet, in kPa
- `p_out`: Pressure at compressor outlet, in kPa
- `t_th_0`: Initial temperature of thermal mass, in Kelvin (*Optional*, default: 315 K)
- `d`: Compressor diameter, in millimeters (*Optional*, default: 34 mm)
- `discretization_steps`: Number of discretization steps (*Optional*, default: 5000)
- `max_error`: Maximum error of compressor model, serves as stop criterion for iterations (*Optional*, default: 0.01)
- `max_iterations`: Maximum number of iterations, serves as stop criterion for iterations (*Optional*, default: 50)


If the simulation is successful, the following properties can be obtained:

- `simulation.eta_sc`: Compressor efficiency
- `simulation.m_dot`: Mass flow rate in kg/s
- `simulation.h_out_mean`: Mean enthalpy at outlet in kJ/kg
- `simulation.t_out_mean`: Mean temperature at outlet in K
