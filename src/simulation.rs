use crate::compressor_parameters::*;
use crate::data::*;
use crate::strokes::*;
use core::panic;
use feos_core::{Contributions, PhaseEquilibrium, StateBuilder, SolverOptions};
use quantity::si::*;

use std::f64::consts;
use std::time::{Duration, Instant};
#[derive(Clone)]
pub struct Iteration {
    pub data: Vec<DataSlice>,
    pub delta_t: f64,
    pub delta_p: f64,
    pub delta_t_th: f64,
    pub error: f64,
}

impl Iteration {
    pub fn new(
        previous_data: DataSlice,
        fluid: &FluidProperties,
        process_param: &ProcessParameters,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        cycle_param: &CycleParameters,
    ) -> Self {
        // Vector Initialization
        let mut all_data: Vec<DataSlice> = Vec::new();
        all_data.push(previous_data);

        // Iteration
        for i in 1..discret_param.steps {
            // Determine phase
            let phase = determine_phase(i, &all_data[i - 1], discret_param.clone(), process_param.clone());
            // Choose behavior according to phase
            if phase == 1 {
                let mut compression = Compression::new(i);
                let data_i = Compression::update(
                    &mut compression,
                    i,
                    &all_data[i - 1],
                    &cycle_param,
                    &compressor_param,
                    &discret_param,
                    &process_param,
                    &fluid,
                )
                .data;
              
                // Add data to vector}
                all_data.push(data_i);
            } else if phase == 2 {
                let mut pushout = PushOut::new(i);
                let data_i = PushOut::update(
                    &mut pushout,
                    i,
                    &all_data[i - 1],
                    &cycle_param,
                    &compressor_param,
                    &discret_param,
                    &process_param,
                    &fluid,
                )
                .data;
    
                // Add data to vector
                all_data.push(data_i);
            } else if phase == 3 {
                let mut expansion = Expansion::new(i);
                let data_i = Expansion::update(
                    &mut expansion,
                    i,
                    &all_data[i - 1],
                    &cycle_param,
                    &compressor_param,
                    &discret_param,
                    &process_param,
                    &fluid,
                )
                .data;
    
                // Add data to vector
                all_data.push(data_i);
            } else if phase == 4 {
                let mut suction = Suction::new(i);
                let data_i = Suction::update(
                    &mut suction,
                    i,
                    &all_data[i - 1],
                    &cycle_param,
                    &compressor_param,
                    &discret_param,
                    &process_param,
                    &fluid,
                )
                .data;
        
                // Add data to vector
                all_data.push(data_i);
            } else {
                panic!()
            }
        }

        // Determine error
        let (delta_t, delta_p, delta_t_th, error) =
            calculate_error(&all_data, &process_param, &compressor_param.t_th_0);
        let last_data = all_data.last().unwrap();

        Self {
            delta_t,
            delta_p,
            delta_t_th,
            error,
            data: all_data,
        }
    }

    pub fn evaluate_iteration(
        &self,
        compressor_param: &CompressorParameters,
        process_param: &ProcessParameters,
        fluid: &FluidProperties,
        discret_param: &DiscretizationParameters,
    ) -> Evaluation {
        let evaluation = Evaluation::new(
            &self.data,
            compressor_param,
            process_param,
            fluid,
            discret_param,
        );
        return evaluation;
    }
}

#[derive(Clone)]
pub struct Simulation {
    pub data: Vec<DataSlice>,
    pub iteration_count: u32,
    pub duration: Duration,
}

impl Simulation {
    pub fn new(
        fluid: &FluidProperties,
        process_param: &ProcessParameters,
        discret_param: &DiscretizationParameters,
        compressor_param: &CompressorParameters,
        target_error: f64,
        max_iterations: u32,
    ) -> Self {
        // Take time
        let start = Instant::now();

        // Initialize cycle parameters
        let mut cycle_param = CycleParameters::new_initial(&compressor_param, &fluid);

        // Initialize previous data with initial state
        let mut previous_data = DataSlice::new_initial(
            &process_param.t_in,
            &process_param.p_in,
            &fluid,
            &discret_param,
            &compressor_param.t_th_0,
        );

        let mut counter: u32 = 0;

        loop {
            // Counter
            counter = counter + 1;
            // Panic if too many iterations have been made
            if counter > max_iterations {
                panic!("Too many iterations.")
            }
            // Conduct iteration
            let iteration = Iteration::new(
                previous_data,
                &fluid,
                &process_param,
                &compressor_param,
                &discret_param,
                &cycle_param,
            );
            let iteration_result = iteration.data;
            let error = iteration.error;
            //print!("Counter: {}, Error: {}\n", counter, error);
            if error < target_error {
                let duration = start.elapsed();
                let time_per_iteration = duration / counter;
                //print!(
                //    "Finished after iteration {}, Final error: {}. Took me: {:?}. Time per iteration: {:?}\n",
                //    counter, error, duration, time_per_iteration
                //);

                return Self {
                    data: iteration_result,
                    iteration_count: counter,
                    duration,
                };
            };

            // Update previous data for next iteration
            previous_data = iteration_result.last().unwrap().clone();
            // Update cycle parameters
            cycle_param = CycleParameters::update(
                &mut cycle_param,
                &compressor_param,
                &discret_param,
                &iteration_result,
            );
        }
    }

    pub fn evaluate_simulation(
        &self,
        compressor_param: &CompressorParameters,
        process_param: &ProcessParameters,
        fluid: &FluidProperties,
        discret_param: &DiscretizationParameters,
    ) -> Evaluation {
        let evaluation = Evaluation::new(
            &self.data,
            compressor_param,
            process_param,
            fluid,
            discret_param,
        );
        return evaluation;
    }
}

pub fn calculate_error(
    data: &Vec<DataSlice>,
    process_param: &ProcessParameters,
    t_th_0: &SINumber,
) -> (f64, f64, f64, f64) {
    let first_data = data.first().unwrap();
    let last_data = data.last().unwrap();

    let delta_t: f64 = ((last_data.temperature - first_data.temperature) / process_param.t_in)
        .into_value()
        .unwrap();
    let delta_p: f64 = ((last_data.pressure - first_data.pressure) / process_param.p_in)
        .into_value()
        .unwrap();
    let delta_t_th: f64 = ((last_data.t_th - first_data.t_th) / t_th_0)
        .into_value()
        .unwrap();

    let error: f64 = delta_t.powi(2).sqrt() + delta_p.powi(2).sqrt() + delta_t_th.powi(2).sqrt();
    //let error: f64 = delta_t.powi(2).sqrt() + delta_p.powi(2).sqrt();
    return (delta_t, delta_p, delta_t_th, error);
}

pub fn determine_phase(
    step: usize,
    data_prev: &DataSlice,
    discret_param: DiscretizationParameters,
    process_param: ProcessParameters,
) -> i32 {
    if discret_param.teta[step] <= consts::PI {
        if data_prev.pressure <= process_param.p_out {
            // Compression
            return 1;
        } else {
            // PushOut
            return 2;
        }
    } else {
        if data_prev.pressure >= process_param.p_in {
            // Expansion
            return 3;
        } else {
            // Suction
            return 4;
        }
    }
}

#[derive(Clone)]
pub struct Evaluation {
    pub m_dot: SINumber,
    pub h_out_mean: SINumber,
    pub t_out_mean: SINumber,
    pub eta_sc: f64,
    pub h_in: f64,
    pub h_out_s: f64,
    pub h_out_mean_disp: f64,
    pub s_in: f64,
}

impl Evaluation {
    pub fn new(
        all_data: &Vec<DataSlice>,
        compressor_parameters: &CompressorParameters,
        process_parameters: &ProcessParameters,
        fluid_properties: &FluidProperties,
        discret_parameters: &DiscretizationParameters,
    ) -> Self {
        // Cells where pushout happens
        let mut total_mass: SINumber = 0.0 * KILOGRAM;
        let mut temperatures_out: Vec<SINumber> = Vec::new();
        let mut angles_out: Vec<f64> = Vec::new();
        let mut enthalpies_out: Vec<SINumber> = Vec::new();
        let mut dm_out: Vec<SINumber> = Vec::new();
        let mut i_out: Vec<usize> = Vec::new();
        for (step, data) in all_data.iter().enumerate() {
            if data.dm < 0.0 * KILOGRAM {
                i_out.push(step);
                total_mass = total_mass + data.dm;
                dm_out.push(-data.dm);
                temperatures_out.push(data.temperature);
                angles_out.push(discret_parameters.teta[step]);
                enthalpies_out.push(data.h);
            };
        }
        total_mass = -total_mass;
        let time_cycle: SINumber = 1. / (compressor_parameters.n);
        let m_dot: SINumber = total_mass / time_cycle;
        let h_out_mean: SINumber = enthalpies_out
            .iter()
            .zip(dm_out.iter())
            .map(|(x, y)| ((x * y) / JOULE).into_value().unwrap())
            .sum::<f64>()
            * JOULE
            / total_mass;

        let t_out_mean: SINumber = temperatures_out
            .iter()
            .zip(dm_out.iter())
            .map(|(x, y)| ((x * y) / KELVIN / KILOGRAM).into_value().unwrap())
            .sum::<f64>()
            * KELVIN
            * KILOGRAM
            / total_mass;

        //print!("Simulation enthalpies: {:?}, Simulation temperatures: {:?}, Mass out: {:?}", enthalpies_out, temperatures_out, dm_out);
        // Compute isentropic state
        // Convert specific to molar entropy
        let state_in = StateBuilder::new(&fluid_properties.eos)
            .temperature(process_parameters.t_in)
            .pressure(process_parameters.p_in)
            .build()
            .unwrap();
        let s_in = state_in.molar_entropy(Contributions::Total);
        let h_in = state_in.specific_enthalpy(Contributions::Total);

        // Find out if isentropic state is in 2-phase-region
        let vlestate = PhaseEquilibrium::pure(
            &fluid_properties.eos,
            process_parameters.p_out,
            None,
            SolverOptions::default(),
        )
        .unwrap();
        let h_bub = vlestate.liquid().specific_enthalpy(Contributions::Total);
        let h_dew = vlestate.vapor().specific_enthalpy(Contributions::Total);
        let s_dew = vlestate.vapor().molar_entropy(Contributions::Total);
        let s_bub = vlestate.liquid().molar_entropy(Contributions::Total);
        let mut h_out_s = 0.0 * KILO * JOULE / KILOGRAM;
        if s_in < s_dew {
            let steam_quality: f64 = ((s_in - s_bub) / (s_dew - s_bub)).into_value().unwrap();
            h_out_s = h_bub + (h_dew - h_bub) * steam_quality;
        } else {
            let state_out_s = StateBuilder::new(&fluid_properties.eos)
                .pressure(process_parameters.p_out)
                .molar_entropy(s_in)
                .build()
                .unwrap();
            h_out_s = state_out_s.specific_enthalpy(Contributions::Total);
        };
        let h_in_display = (h_in / (KILO * JOULE / KILOGRAM)).into_value().unwrap();
        let s_in_display = (s_in / (KILO * JOULE / KELVIN / MOL)).into_value().unwrap();
        let h_out_s_display = (h_out_s / (KILO * JOULE / KILOGRAM)).into_value().unwrap();
        let h_out_mean_display = (h_out_mean / (KILO * JOULE / KILOGRAM))
            .into_value()
            .unwrap();

        let eta_sc: f64 = ((h_out_s - h_in) / (h_out_mean - h_in))
            .into_value()
            .unwrap();
        Self {
            eta_sc,
            h_out_mean,
            t_out_mean,
            m_dot,
            h_in: h_in_display,
            h_out_mean_disp: h_out_mean_display,
            h_out_s: h_out_s_display,
            s_in: s_in_display,
        }
    }
}
