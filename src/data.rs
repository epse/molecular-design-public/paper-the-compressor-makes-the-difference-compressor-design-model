use crate::compressor_parameters::*;
use feos_core::{Contributions, StateBuilder};
use quantity::si::*;
use std::usize;
#[derive(Clone, Copy)]
pub struct DataSlice {
    pub step: usize,
    pub temperature: SINumber,
    pub pressure: SINumber,
    pub specific_volume: SINumber,
    pub u: SINumber,
    pub h: SINumber,
    pub s: SINumber,
    pub m: SINumber,
    pub t_th: SINumber,
    pub alpha: SINumber,
    pub dm: SINumber,
    pub q: SINumber,
}

impl DataSlice {
    pub fn new_empty(step: usize) -> Self {
        Self {
            step,
            temperature: 0.0 * KELVIN,
            pressure: 0.0 * KILO * PASCAL,
            specific_volume: 0.0 * (METER * METER * METER / KILOGRAM),
            u: 0.0 * KILO * JOULE / KILOGRAM,
            h: 0.0 * KILO * JOULE / KILOGRAM,
            s: 0.0 * KILO * JOULE / (KILOGRAM * KELVIN),
            m: 0.0 * KILOGRAM,
            t_th: 0.0 * KELVIN,
            alpha: 0.0 * (WATT / (METER * METER * KELVIN)),
            dm: 0.0 * KILOGRAM,
            q: 0.0 * KILO * JOULE,
        }
    }

    pub fn new(
        step: usize,
        specific_volume: &SINumber,
        u: &SINumber,
        t_0: &SINumber,
        m: &SINumber,
        t_th: &SINumber,
        alpha: &SINumber,
        dm: &SINumber,
        q: &SINumber,
        fluid: &FluidProperties,
    ) -> Self {
        // create state from u and v
        let molar_mass: SINumber = fluid.molarweight * GRAM / MOL;
        let volume: SINumber = molar_mass * specific_volume * MOL;
        let molar_u: SINumber = molar_mass * u;
        let total_moles: SINumber = 1.0 * MOL;
        let state = StateBuilder::new(&fluid.eos)
            .volume(volume)
            .total_moles(total_moles)
            .molar_internal_energy(molar_u)
            .initial_temperature(*t_0)
            .build()
            .unwrap();

    
        let temperature = state.temperature;
        let pressure = state.pressure(Contributions::Total);
        let h = state.specific_enthalpy(Contributions::Total);
        let s = state.specific_entropy(Contributions::Total);
        Self {
            step,
            temperature,
            pressure,
            specific_volume: *specific_volume,
            u: *u,
            h,
            s,
            m: *m,
            t_th: *t_th,
            alpha: *alpha,
            dm: *dm,
            q: *q,
        }
    }

    pub fn new_initial(
        t_in: &SINumber,
        p_in: &SINumber,
        fluid: &FluidProperties,
        discret_param: &DiscretizationParameters,
        t_th_0: &SINumber,
    ) -> Self {
        let state_in = StateBuilder::new(&fluid.eos)
            .temperature(*t_in)
            .pressure(*p_in)
            .build()
            .unwrap();
        let initial_volume: SINumber = discret_param.volume[0];
        let initial_specific_volume: SINumber = 1.0 / state_in.mass_density();
        let initial_mass: SINumber = initial_volume / initial_specific_volume;
        Self {
            step: 0usize,
            temperature: *t_in,
            pressure: *p_in,
            specific_volume: initial_specific_volume,
            u: state_in.specific_internal_energy(Contributions::Total),
            h: state_in.specific_enthalpy(Contributions::Total),
            s: state_in.specific_entropy(Contributions::Total),
            m: initial_mass,
            t_th: *t_th_0,
            alpha: 0.0 * (WATT / (METER * METER * KELVIN)),
            dm: 0.0 * KILOGRAM,
            q: 0.0 * KILO * JOULE,
        }
    }
}
