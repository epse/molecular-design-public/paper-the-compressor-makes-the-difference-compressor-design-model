use crate::compressor_parameters::*;
use crate::data::*;
use ndarray::Array1;
use quantity::si::*;
use std::f64::consts;

pub trait Balance {
    fn mass_balance(
        &self,
        step: usize,
        data_prev: &DataSlice,
        discret_param: &DiscretizationParameters,
        cycle_param: &CycleParameters,
        process_param: &ProcessParameters,
        compressor_param: &CompressorParameters,
    ) -> (SINumber, SINumber, SINumber) {
        let dm: SINumber = self.dm(
            step,
            discret_param,
            cycle_param,
            process_param,
            compressor_param,
            data_prev,
        );
        let m: SINumber = data_prev.m + dm;
        let v: SINumber = discret_param.volume[step] / m;
        (m, dm, v)
    }

    // only valid for suction phase, others have to be implemented in trait implementation
    fn m_dot(
        &self,
        cycle_param: &CycleParameters,
        process_param: &ProcessParameters,
        data_prev: &DataSlice,
    ) -> SINumber {
        let m_dot: SINumber = (cycle_param.a_eff_in / process_param.v_in)
            * (2.0 * (process_param.p_in - data_prev.pressure) * process_param.v_in)
                .sqrt()
                .unwrap();
        m_dot
    }

    // only valid for suction phase, others have to be implemented in trait implementation
    fn dm(
        &self,
        step: usize,
        discret_param: &DiscretizationParameters,
        cycle_param: &CycleParameters,
        process_param: &ProcessParameters,
        compressor_param: &CompressorParameters,
        data_prev: &DataSlice,
    ) -> SINumber {
        let m_dot: SINumber = self.m_dot(cycle_param, process_param, data_prev);
        let dm: SINumber = m_dot
            * ((discret_param.teta[step] - discret_param.teta[step - 1])
                / (2.0 * consts::PI * compressor_param.n));
        dm
    }

    // only valid for suction phase, others have to be implemented in trait implementation
    fn enthalpy_dm(&self, process_param: &ProcessParameters, data_prev: &DataSlice) -> SINumber {
        process_param.h_in
    }

    fn energy_balance(
        &self,
        step: usize,
        data_prev: &DataSlice,
        dm: SINumber,
        m: SINumber,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        process_param: &ProcessParameters,
    ) -> (SINumber, SINumber, SINumber, SINumber) {
        let work: SINumber = self.work(step, &data_prev.pressure, &discret_param.volume);
        let w_fric: SINumber = self.friction(step, compressor_param, &discret_param.volume);
        let (q_loss, alpha): (SINumber, SINumber) =
            self.q_loss(step, data_prev, discret_param, compressor_param);
        let h: SINumber = self.enthalpy_dm(process_param, data_prev);
        let u: SINumber = (work + w_fric + q_loss + dm * h + data_prev.u * data_prev.m) / m;
        let t_th: SINumber = self.t_th(step, q_loss, compressor_param, discret_param, data_prev);
        (u, t_th, alpha, q_loss)
    }

    fn friction(
        &self,
        step: usize,
        compressor_param: &CompressorParameters,
        volume: &Array1<SINumber>,
    ) -> SINumber {
        let w_fric: SINumber = compressor_param.p_fr * (volume[step] - volume[step - 1]);
        w_fric.abs()
    }

    fn work(&self, step: usize, pressure_prev: &SINumber, volume: &Array1<SINumber>) -> SINumber {
        let w: SINumber = -pressure_prev * (volume[step] - volume[step - 1]);
        w
    }

    fn alpha(
        &self,
        step: usize,
        data_prev: &DataSlice,
        discret_param: &DiscretizationParameters,
        compressor_param: &CompressorParameters,
    ) -> SINumber {
        let k = self.k_valve();
        let v_p: f64 = ((discret_param.x[step] - discret_param.x[step - 1])
            / ((discret_param.teta[step] - discret_param.teta[step - 1])
                / (2.0 * consts::PI * compressor_param.n))
            / (METER / SECOND))
            .into_value()
            .unwrap()
            .abs();
        let d: f64 = (compressor_param.d / METER).into_value().unwrap();
        let p: f64 = (data_prev.pressure / BAR).into_value().unwrap();
        let t: f64 = (data_prev.temperature / (KELVIN)).into_value().unwrap();
        let alpha: SINumber = 127.93
            * d.powf(-0.2)
            * p.powf(0.8)
            * t.powf(-0.55)
            * (k * v_p).powf(0.8)
            * (WATT / (METER * METER * KELVIN));
        alpha
    }

    fn k_valve(&self) -> f64;

    fn q_loss(
        &self,
        step: usize,
        data_prev: &DataSlice,
        discret_param: &DiscretizationParameters,
        compressor_param: &CompressorParameters,
    ) -> (SINumber, SINumber) {
        let alpha = self.alpha(step, &data_prev, discret_param, compressor_param);
        let t_th_prev: SINumber = data_prev.t_th;
        let t_prev: SINumber = data_prev.temperature;
        let q_loss: SINumber = alpha
            * discret_param.a_wall[step]
            * (t_th_prev - t_prev)
            * ((discret_param.teta[step] - discret_param.teta[step - 1])
                / (2.0 * consts::PI * compressor_param.n));
        (q_loss, alpha)
    }

    fn q_env(
        &self,
        step: usize,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        t_th_prev: SINumber,
    ) -> SINumber {
        let q_env: SINumber = compressor_param.alpha_env
            * compressor_param.a_env
            * (compressor_param.t_env - t_th_prev)
            * ((discret_param.teta[step] - discret_param.teta[step - 1])
                / (2.0 * consts::PI * compressor_param.n));
        q_env
    }

    fn t_th(
        &self,
        step: usize,
        q_loss: SINumber,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        data_prev: &DataSlice,
    ) -> SINumber {
        let q_env: SINumber = self.q_env(step, compressor_param, discret_param, data_prev.t_th);
        let t_th: SINumber =
            data_prev.t_th + (q_env - q_loss) / (compressor_param.m * compressor_param.c_v);
        t_th
    }

    fn conduct_balance(
        &self,
        step: usize,
        data_prev: &DataSlice,
        cycle_param: &CycleParameters,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        process_param: &ProcessParameters,
        fluid: &FluidProperties,
    ) -> DataSlice {
        // Mass balance
        let (m, dm, v) = self.mass_balance(
            step,
            data_prev,
            discret_param,
            cycle_param,
            process_param,
            compressor_param,
        );
        // Energy balance
        let (u, t_th, alpha, q_loss) = self.energy_balance(
            step,
            data_prev,
            dm,
            m,
            compressor_param,
            discret_param,
            process_param,
        );
        // Create new Data Slice
        let data_now = DataSlice::new(
            step,
            &v,
            &u,
            &data_prev.temperature,
            &m,
            &t_th,
            &alpha,
            &dm,
            &q_loss,
            fluid,
        );
        data_now
    }
}
