#![allow(clippy::too_many_arguments)]
pub mod balances;
pub mod compressor_parameters;
pub mod data;
pub mod simulation;
pub mod strokes;

use pyo3::exceptions::PyRuntimeError;
use pyo3::prelude::*;
use pyo3::wrap_pyfunction;
use pyo3::wrap_pymodule;
use pyo3::PyErr;

pub mod pysimulation;

#[pymodule]
pub fn detailed_compressor(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<pysimulation::PySimulation>()?;
    m.add_class::<pysimulation::PyIteration>()?;
    Ok(())
}
