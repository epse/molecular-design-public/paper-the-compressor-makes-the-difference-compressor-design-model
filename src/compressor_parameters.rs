use crate::data::*;
use feos_core::joback::JobackRecord;
use feos_core::parameter::Parameter;
use feos_core::parameter::{Identifier, PureRecord};
use feos_core::{Contributions, StateBuilder};
use feos::pcsaft::PcSaftParameters;
use feos::pcsaft::{PcSaft, PcSaftOptions, PcSaftRecord};
use ndarray::Array1;
use quantity::si::*;
use std::f64::consts;
use std::sync::Arc;
use std::usize;

pub struct FluidProperties {
    pub molarweight: f64,
    pub eos: Arc<PcSaft>,
}

impl FluidProperties {
    pub fn new(parameters: PcSaftParameters) -> Self {
        let molarweight = parameters.molarweight[0];
        let eos = PcSaft::with_options(Arc::new(parameters), PcSaftOptions::default());

        Self {
            molarweight,
            eos: Arc::new(eos),
        }
    }

    pub fn new_from_param_values(saft_values: Vec<f64>, joback_values: Option<Vec<f64>>) -> Self {
        // PcSaft parameters from vector
        let id: Identifier = Identifier::new(None, None, None, None, None, None);
        let mw = saft_values[6];
        let sr: PcSaftRecord = PcSaftRecord::new(
            saft_values[0],
            saft_values[1],
            saft_values[2],
            Some(saft_values[3]),
            None,
            Some(saft_values[4]),
            Some(saft_values[5]),
            None,
            None,
            None,
            None,
            None,
        );
        let mut jr = None;
        match joback_values {
            Some(values) => {
                jr = Some(JobackRecord::new(
                    values[0], values[1], values[2], values[3], values[4],
                ));
            }
            None => jr = None,
        };
        let pr = PureRecord {
            identifier: id,
            model_record: sr,
            molarweight: mw,
            ideal_gas_record: jr,
        };

        let saft_parameters: PcSaftParameters = PcSaftParameters::new_pure(pr);
        let eos = PcSaft::with_options(Arc::new(saft_parameters), PcSaftOptions::default());

        Self {
            molarweight: mw,
            eos: Arc::new(eos),
        }
    }
}

#[derive(Debug, Clone)]
pub struct CompressorParameters {
    pub d: SINumber,
    pub l_to_a: f64,
    pub h: SINumber,
    pub a_env: SINumber,
    pub p_fr: SINumber,
    pub f: SINumber,
    pub n: SINumber,
    pub c_cl: f64,
    pub d_0: SINumber,
    pub h_0: SINumber,
    pub n_piston_0: i32,
    pub a_env_0: SINumber,
    pub m: SINumber,
    pub c_v: SINumber,
    pub alpha_env: SINumber,
    pub t_env: SINumber,
    pub t_th_0: SINumber,
}

impl CompressorParameters {
    pub fn new(t_th_0: Option<f64>, d: Option<f64>) -> Self {
        let d_0 = 34.0 * MILLI * METER;
        let d = d.unwrap_or(34.0) * MILLI * METER;
        let d_to_h = 1.25;
        let h = d / d_to_h;
        let h_0 = 34.0 * MILLI * METER / d_to_h;
        let a_env_0 = 0.04 * METER * METER;
        let a_env = a_env_0 * (d.powi(2) * h) / (d_0.powi(2) * h_0);
        let p_fr_0 = 80.12 * KILO * PASCAL;
        let f = 0.5;
        let p_fr = p_fr_0
            * (f * ((d_0.powi(2) * h_0) / (d.powi(2) * h)).value().unwrap()
                + (1.0 - f) * (d_0 / d).value().unwrap());
        let c_cl_0 = 0.1;
        let c_cl = c_cl_0 * (h_0 / h).value().unwrap();

        Self {
            d,
            l_to_a: 3.5,
            h,
            a_env,
            p_fr,
            f: 50.0 * HERTZ,
            n: 25.0 * HERTZ,
            c_cl,
            d_0,
            h_0,
            n_piston_0: 2,
            a_env_0,
            m: 0.0001 * KILOGRAM,
            c_v: 0.502 * KILO * JOULE / (KILOGRAM * KELVIN),
            alpha_env: 5.0 * (WATT / (METER * METER * KELVIN)),
            t_env: 298.15 * KELVIN,
            t_th_0: t_th_0.unwrap_or(315.0) * KELVIN,
        }
    }
}

#[derive(Debug, Clone)]
pub struct ProcessParameters {
    pub t_in: SINumber,
    pub p_in: SINumber,
    pub h_in: SINumber,
    pub p_out: SINumber,
    pub v_in: SINumber,
}

impl ProcessParameters {
    pub fn new(
        t_in: Option<SINumber>,
        p_in: Option<SINumber>,
        p_out: Option<SINumber>,
        fluid: &FluidProperties,
    ) -> Self {
        let tin = t_in.unwrap_or(290.0 * KELVIN);
        let pin = p_in.unwrap_or(625.0 * KILO * PASCAL);
        let pout = p_out.unwrap_or(1170.0 * KILO * PASCAL);
        let state_in = StateBuilder::new(&fluid.eos)
            .temperature(tin)
            .pressure(pin)
            .total_moles(1.0 * MOL)
            .build()
            .unwrap();
        let vin = 1.0 / state_in.mass_density();
        let hin = state_in.specific_enthalpy(Contributions::Total);
        Self {
            t_in: tin,
            p_in: pin,
            h_in: hin,
            p_out: pout,
            v_in: vin,
        }
    }
}

#[derive(Debug, Clone)]
pub struct DiscretizationParameters {
    pub steps: usize,
    pub teta: Array1<f64>,
    pub x: Array1<SINumber>,
    pub volume: Array1<SINumber>,
    pub a_wall: Array1<SINumber>,
}

impl DiscretizationParameters {
    pub fn new(steps: usize, compressor_parameters: &CompressorParameters) -> Self {
        let teta: Array1<f64> = Array1::linspace(0., 2.0 * consts::PI, steps);
        let x: Array1<SINumber> = teta
            .map(|i| DiscretizationParameters::teta_to_x(i, compressor_parameters));
        let a_head: SINumber =
            0.25 * consts::PI * compressor_parameters.d * compressor_parameters.d;
        let volume: Array1<SINumber> = x.map(|i| i * a_head);
        let a_wall: Array1<SINumber> = x.map(|i| {
            (consts::PI * (i / MILLI / METER) * compressor_parameters.d / MILLI / METER)
                * MILLI
                * METER
                * MILLI
                * METER
                + 2.0 * a_head
        });

        Self {
            steps,
            teta,
            x,
            a_wall,
            volume,
        }
    }

    pub fn teta_to_x(teta: &f64, compressor: &CompressorParameters) -> SINumber {
        let x: SINumber = -0.5
            * compressor.h
            * (1.0 - teta.cos()
                + compressor.l_to_a
                    * (1.0 - (1.0 - (1.0 / compressor.l_to_a * teta.sin()).powi(2)).sqrt()))
            + compressor.c_cl * compressor.h
            + compressor.h;
        x
    }
}

#[derive(Clone, Copy)]
pub struct CycleParameters {
    pub a_eff_in: SINumber,
    pub a_eff_out: SINumber,
}

impl CycleParameters {
    pub fn new(
        compressor_parameters: &CompressorParameters,
        fluid_properties: &FluidProperties,
        a_eff_out: &SINumber,
    ) -> Self {
        let rmolar = 8.3144598;
        let molar_weight = fluid_properties.molarweight * 1e-3;

        let a_eff_in: SINumber = (2.0415e-3
            * (rmolar / molar_weight).powf(-0.9826)
            * (compressor_parameters.d / compressor_parameters.d_0).powi(2))
            * METER
            * METER;

        Self {
            a_eff_in,
            a_eff_out: *a_eff_out,
        }
    }

    pub fn new_initial(
        compressor_parameters: &CompressorParameters,
        fluid_properties: &FluidProperties,
    ) -> Self {
        let rmolar = 8.3144598;
        let molar_weight = fluid_properties.molarweight * 1e-3;

        let a_eff_in: SINumber = (2.0415e-3
            * (rmolar / molar_weight).powf(-0.9826)
            * (compressor_parameters.d / compressor_parameters.d_0).powi(2))
            * METER
            * METER;
        let a_eff_out: SINumber =
            1.5e-5 * (compressor_parameters.d / compressor_parameters.d_0).powi(2) * METER * METER;

        Self {
            a_eff_in,
            a_eff_out,
        }
    }

    pub fn update(
        &mut self,
        compressor_parameters: &CompressorParameters,
        discret_parameters: &DiscretizationParameters,
        iteration_result: &[DataSlice]
    ) -> Self {
        // Cells where pushout happens
        let mut total_mass: SINumber = 0.0 * KILOGRAM;
        let mut temperatures_out: Vec<SINumber> = Vec::new();
        let mut angles_out: Vec<f64> = Vec::new();
        for (i, data) in iteration_result.iter().enumerate() {
            if data.dm < 0.0 * KILOGRAM {
                total_mass += data.dm;
                temperatures_out.push(data.temperature);
                angles_out.push(discret_parameters.teta[i]);
            };
        }
        total_mass = -total_mass;

        let time_out = (angles_out.last().unwrap() - angles_out.first().unwrap())
            / (2.0 * consts::PI * compressor_parameters.n);

        let mass_flow_density: f64 = ((total_mass / time_out / self.a_eff_out)
            / (KILOGRAM / (METER * METER * SECOND)))
            .into_value()
            .unwrap();

        self.a_eff_out = 5.1109e-4
            * mass_flow_density.powf(-0.486)
            * (compressor_parameters.d / compressor_parameters.d_0).powi(2)
            * METER
            * METER;

        *self
    }
}
