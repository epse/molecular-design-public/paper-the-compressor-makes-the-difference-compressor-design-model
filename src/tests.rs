use crate::data::*;
use crate::pysimulation::*;
use crate::simulation::*;
use crate::strokes::*;
use crate::{compressor_parameters::*, simulation::*};
use approx::assert_relative_eq;
use feos_core::{Contributions, State, StateBuilder};
use feos_pcsaft::parameters::utils;
use feos_pcsaft::PcSaft;
use std::error::Error;
use std::rc::Rc;
use std::time::{Duration, Instant};

//#[test]
//fn iteration() -> Result<(), Box<dyn Error>> {
//
//    let p = utils::propane_parameters();
//    let fluid = FluidProperties::new(p);
//    let process_param = ProcessParameters::new(None, None, None, &fluid);
//    let compressor_param = CompressorParameters::new();
//    let discret_param = DiscretizationParameters::new(100, &compressor_param);
//    let cycle_param = CycleParameters::new_initial(&compressor_param, &fluid);
//    let data = DataSlice::new_initial(&process_param, &fluid, &discret_param, &compressor_param);
//
//    let iteration = Iteration::new(100usize, data, &fluid, &process_param, &compressor_param, &discret_param, &cycle_param);
//
//    let error = simulation::calculate_error(&iteration.data);
//
//    print!("{}", error);
//
//    Ok(())
//
//}

#[test]
fn simulation() -> Result<(), Box<dyn Error>> {
    let p = utils::propane_parameters();
    let fluid = FluidProperties::new(&p);
    let process_param = ProcessParameters::new(None, None, None, &fluid);
    let compressor_param = CompressorParameters::new(None);
    let discret_param = DiscretizationParameters::new(100, &compressor_param);

    let simulation = Simulation::new(
        &fluid,
        &process_param,
        &discret_param,
        &compressor_param,
        0.01,
        50u32,
    );

    let evaluation = Simulation::evaluate_simulation(
        &simulation,
        &compressor_param,
        &process_param,
        &fluid,
        &discret_param,
    );
    print!(
        "Mass flow: {},  eta_sC: {}",
        evaluation.m_dot, evaluation.eta_sc
    );

    Ok(())
}

#[test]
fn pysimulation() -> Result<(), Box<dyn Error>> {
    let p = utils::propane_parameters();
    let fluid = FluidProperties::new(&p);
    let process_param = ProcessParameters::new(None, None, None, &fluid);
    let compressor_param = CompressorParameters::new(None);
    let discret_param = DiscretizationParameters::new(100, &compressor_param);

    let pysimulation = PySimulation::new(
        Vec::new(),
        None,
        290.0,
        625.0,
        1170.0,
        Some(315.0),
        Some(5000),
        Some(0.01),
        Some(50u32),
    );

    Ok(())
}
