use crate::simulation::*;
use crate::{compressor_parameters::*, data::DataSlice};
use quantity::si::*;

//use numpy::convert::ToPyArray;
//use numpy::PyArray2;
use pyo3::prelude::*;

#[pyclass(module = "detailed-compressor", name = "Simulation", unsendable)]
#[pyo3(text_signature = "(saft_parameter_values, t_in, p_in, p_out, discretization_steps=None)")]
#[derive(Clone)]
pub struct PySimulation {
    pub eta_sc: f64,
    pub m_dot: f64,
    pub h_out_mean: f64,
    pub t_out_mean: f64,
    pub h_in: f64,
    pub h_out_s: f64,
    pub h_out_mean_disp: f64,
    pub s_in: f64,
}

#[pymethods]
impl PySimulation {
    #[new]
    pub fn new(
        saft_param_values: Vec<f64>,
        joback_param_values: Option<Vec<f64>>,
        t_in: f64,
        p_in: f64,
        p_out: f64,
        t_th_0: Option<f64>,
        d: Option<f64>,
        discretization_steps: Option<i32>,
        max_error: Option<f64>,
        max_iterations: Option<u32>,
    ) -> PyResult<Self> {
        // Unpack function parameters
        let steps: usize = discretization_steps.unwrap_or(5000) as usize;
        let target_error: f64 = max_error.unwrap_or(0.01);
        let maximum_iterations: u32 = max_iterations.unwrap_or(50);

        // Create parameters
        // Compressor parameters
        let compressor_param = CompressorParameters::new(t_th_0, d);
        // Discretization parameters
        let discret_param = DiscretizationParameters::new(steps, &compressor_param);
        // Fluid parameters
        let fluid = FluidProperties::new_from_param_values(saft_param_values, joback_param_values);
        // Process parameters
        let process_param = ProcessParameters::new(
            Some(t_in * KELVIN),
            Some(p_in * KILO * PASCAL),
            Some(p_out * KILO * PASCAL),
            &fluid,
        );
        // Create simulation
        let simulation = Simulation::new(
            &fluid,
            &process_param,
            &discret_param,
            &compressor_param,
            target_error,
            maximum_iterations,
        );

        // Evaluate simulation
        let evaluation = Simulation::evaluate_simulation(
            &simulation,
            &compressor_param,
            &process_param,
            &fluid,
            &discret_param,
        );

        Ok(Self {
            eta_sc: evaluation.eta_sc,
            m_dot: (evaluation.m_dot / (KILOGRAM / SECOND))
                .into_value()
                .unwrap(),
            h_out_mean: (evaluation.h_out_mean / (KILO * JOULE / KILOGRAM))
                .into_value()
                .unwrap(),
            t_out_mean: (evaluation.t_out_mean / KELVIN).into_value().unwrap(),
            h_in: evaluation.h_in,
            h_out_s: evaluation.h_out_s,
            h_out_mean_disp: evaluation.h_out_mean_disp,
            s_in: evaluation.s_in,
        })
    }

    #[getter]
    fn get_eta_sc(&self) -> f64 {
        self.eta_sc
    }
    #[getter]
    fn get_m_dot(&self) -> f64 {
        self.m_dot
    }
    #[getter]
    fn get_h_out(&self) -> f64 {
        self.h_out_mean
    }
    #[getter]
    fn get_t_out(&self) -> f64 {
        self.t_out_mean
    }
    #[getter]
    fn get_h_in(&self) -> f64 {
        self.h_in
    }
    #[getter]
    fn get_h_out_s(&self) -> f64 {
        self.h_out_s
    }
    #[getter]
    fn get_h_out_mean_disp(&self) -> f64 {
        self.h_out_mean_disp
    }
    #[getter]
    fn get_s_in(&self) -> f64 {
        self.s_in
    }
}

#[pyclass(module = "detailed-compressor", name = "Iteration", unsendable)]
#[pyo3(
    text_signature = "(saft_parameter_values, t_in, p_in, p_out, t_0, p_0, t_th_0, a_eff_out, discretization_steps=None)"
)]
#[derive(Clone)]
pub struct PyIteration {
    pub delta_t: f64,
    pub delta_p: f64,
    pub delta_t_th: f64,
    pub eta_sc: f64,
    pub m_dot: f64,
    pub h_out_mean: f64,
    pub t_out_mean: f64,
}

#[pymethods]
impl PyIteration {
    #[new]
    fn new(
        saft_param_values: Vec<f64>,
        joback_param_values: Option<Vec<f64>>,
        t_in: f64,
        p_in: f64,
        p_out: f64,
        t_0: f64,
        p_0: f64,
        t_th_0: f64,
        d: f64,
        a_eff_out: f64,
        discretization_steps: Option<i32>,
    ) -> PyResult<Self> {
        // Unpack function parameters
        let steps: usize = discretization_steps.unwrap_or(5000) as usize;

        // Create parameters
        // Compressor parameters
        let compressor_param = CompressorParameters::new(Some(t_th_0), Some(d));
        // Discretization parameters
        let discret_param = DiscretizationParameters::new(steps, &compressor_param);
        // Fluid parameters
        let fluid = FluidProperties::new_from_param_values(saft_param_values, joback_param_values);
        // Process parameters
        let process_param = ProcessParameters::new(
            Some(t_in * KELVIN),
            Some(p_in * KILO * PASCAL),
            Some(p_out * KILO * PASCAL),
            &fluid,
        );
        // Create cycle parameters
        let cycle_param =
            CycleParameters::new(&compressor_param, &fluid, &(a_eff_out * METER * METER));
        // Create initial data slice
        let data_prev = DataSlice::new_initial(
            &(t_0 * KELVIN),
            &(p_0 * KILO * PASCAL),
            &fluid,
            &discret_param,
            &(t_th_0 * KELVIN),
        );

        // Iteration
        let iteration = Iteration::new(
            data_prev,
            &fluid,
            &process_param,
            &compressor_param,
            &discret_param,
            &cycle_param,
        );
        // Evaluate simulation
        let evaluation = Iteration::evaluate_iteration(
            &iteration,
            &compressor_param,
            &process_param,
            &fluid,
            &discret_param,
        );

        Ok(Self {
            eta_sc: evaluation.eta_sc,
            m_dot: (evaluation.m_dot / (KILOGRAM / SECOND))
                .into_value()
                .unwrap(),
            h_out_mean: (evaluation.h_out_mean / (KILO * JOULE / KILOGRAM))
                .into_value()
                .unwrap(),
            t_out_mean: (evaluation.t_out_mean / KELVIN).into_value().unwrap(),
            delta_t: iteration.delta_t,
            delta_p: iteration.delta_p,
            delta_t_th: iteration.delta_t_th,
        })
    }

    #[getter]
    fn get_eta_sc(&self) -> f64 {
        self.eta_sc
    }

    #[getter]
    fn get_m_dot(&self) -> f64 {
        self.m_dot
    }

    #[getter]
    fn get_h_out(&self) -> f64 {
        self.h_out_mean
    }

    #[getter]
    fn get_t_out(&self) -> f64 {
        self.t_out_mean
    }

    #[getter]
    fn delta_t(&self) -> f64 {
        self.delta_t
    }

    #[getter]
    fn delta_p(&self) -> f64 {
        self.delta_p
    }

    #[getter]
    fn delta_t_th(&self) -> f64 {
        self.delta_t_th
    }
}
