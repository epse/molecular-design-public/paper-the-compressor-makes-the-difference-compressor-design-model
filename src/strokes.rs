use crate::balances::*;
use crate::compressor_parameters::*;
use crate::data::*;
use quantity::si::*;
use std::f64::consts;
//use std::{error::Error, iter::Cycle};

pub struct Compression {
    pub data: DataSlice,
}

impl Balance for Compression {
    fn k_valve(&self) -> f64 {
        let k_valve = 2.28;
        k_valve
    }

    fn dm(
        &self,
        step: usize,
        discret_param: &DiscretizationParameters,
        cycle_param: &CycleParameters,
        process_param: &ProcessParameters,
        compressor_param: &CompressorParameters,
        data_prev: &DataSlice,
    ) -> SINumber {
        0.0 * KILOGRAM
    }

    fn enthalpy_dm(&self, process_param: &ProcessParameters, data_prev: &DataSlice) -> SINumber {
        0.0 * KILO * JOULE / KILOGRAM
    }
}

impl Compression {
    pub fn new(step: usize) -> Self {
        let data = DataSlice::new_empty(step);
        Self { data }
    }
    pub fn update(
        &mut self,
        step: usize,
        data_prev: &DataSlice,
        cycle_param: &CycleParameters,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        process_param: &ProcessParameters,
        fluid: &FluidProperties,
    ) -> Self {
        let data = self.conduct_balance(
            step,
            data_prev,
            cycle_param,
            compressor_param,
            discret_param,
            process_param,
            fluid,
        );
        Self { data }
    }
}

pub struct Suction {
    pub data: DataSlice,
}

impl Balance for Suction {
    fn k_valve(&self) -> f64 {
        let k_valve = 5.18;
        k_valve
    }
}

impl Suction {
    pub fn new(step: usize) -> Self {
        let data = DataSlice::new_empty(step);
        Self { data }
    }
    pub fn update(
        &mut self,
        step: usize,
        data_prev: &DataSlice,
        cycle_param: &CycleParameters,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        process_param: &ProcessParameters,
        fluid: &FluidProperties,
    ) -> Self {
        let data = self.conduct_balance(
            step,
            data_prev,
            cycle_param,
            compressor_param,
            discret_param,
            process_param,
            fluid,
        );
        Self { data }
    }
}

pub struct Expansion {
    pub data: DataSlice,
}

impl Balance for Expansion {
    fn k_valve(&self) -> f64 {
        let k_valve = 2.28;
        k_valve
    }

    fn dm(
        &self,
        step: usize,
        discret_param: &DiscretizationParameters,
        cycle_param: &CycleParameters,
        process_param: &ProcessParameters,
        compressor_param: &CompressorParameters,
        data_prev: &DataSlice,
    ) -> SINumber {
        0.0 * KILOGRAM
    }

    fn enthalpy_dm(&self, process_param: &ProcessParameters, data_prev: &DataSlice) -> SINumber {
        0.0 * KILO * JOULE / KILOGRAM
    }
}

impl Expansion {
    pub fn new(step: usize) -> Self {
        let data = DataSlice::new_empty(step);
        Self { data }
    }
    pub fn update(
        &mut self,
        step: usize,
        data_prev: &DataSlice,
        cycle_param: &CycleParameters,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        process_param: &ProcessParameters,
        fluid: &FluidProperties,
    ) -> Self {
        let data = self.conduct_balance(
            step,
            data_prev,
            cycle_param,
            compressor_param,
            discret_param,
            process_param,
            fluid,
        );
        Self { data }
    }
}

pub struct PushOut {
    pub data: DataSlice,
}

impl Balance for PushOut {
    fn k_valve(&self) -> f64 {
        let k_valve = 5.18;
        k_valve
    }

    fn m_dot(
        &self,
        cycle_param: &CycleParameters,
        process_param: &ProcessParameters,
        data_prev: &DataSlice,
    ) -> SINumber {
        let m_dot: SINumber = (cycle_param.a_eff_out / data_prev.specific_volume)
            * (2.0 * (data_prev.pressure - process_param.p_out) * data_prev.specific_volume)
                .sqrt()
                .unwrap();
        m_dot
    }

    fn dm(
        &self,
        step: usize,
        discret_param: &DiscretizationParameters,
        cycle_param: &CycleParameters,
        process_param: &ProcessParameters,
        compressor_param: &CompressorParameters,
        data_prev: &DataSlice,
    ) -> SINumber {
        let m_dot: SINumber = self.m_dot(cycle_param, process_param, data_prev);
        let dm: SINumber = m_dot
            * ((discret_param.teta[step] - discret_param.teta[step - 1])
                / (2.0 * consts::PI * compressor_param.n));

        -dm
    }

    fn enthalpy_dm(&self, process_param: &ProcessParameters, data_prev: &DataSlice) -> SINumber {
        let enthalpy = data_prev.h;
        enthalpy
    }
}

impl PushOut {
    pub fn new(step: usize) -> Self {
        let data = DataSlice::new_empty(step);
        Self { data }
    }
    pub fn update(
        &mut self,
        step: usize,
        data_prev: &DataSlice,
        cycle_param: &CycleParameters,
        compressor_param: &CompressorParameters,
        discret_param: &DiscretizationParameters,
        process_param: &ProcessParameters,
        fluid: &FluidProperties,
    ) -> Self {
        let data = self.conduct_balance(
            step,
            data_prev,
            cycle_param,
            compressor_param,
            discret_param,
            process_param,
            fluid,
        );
        Self { data }
    }
}
